package entities;

import java.util.List;

public class FooComplexList {
	private List<Foo> list;

	public List<Foo> getList() {
		return list;
	}

	public void setList(List<Foo> list) {
		this.list = list;
	}
}
